import datetime, time, requests, socket, logging, threading

from src.collections import Collections
from src.database import Mongo, Influx
from src.gravar_dados import Gravar
from src.variaveis import info_mongo

def _tempo_atual():
    t = time.time()
    return datetime.datetime.fromtimestamp(t).strftime('%d/%m/%Y %H:%M:%S')

def _total_sites():
    return len(info_mongo)

def gravar_log(message):
    logging.info(message)

def checar_conexao_mongo(ip, porta, mongo_name):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((ip, int(porta)))
    if result == 0:
        # Informando a API que o MongoDB está tudo bem!
        status_mongo = {"status": "online", "description": "success", 'lastUpdate': _tempo_atual()}
        post_api('mongodb', mongo_name, status_mongo)
        return False
    # Informando a API que o MongoDB está com algum problema...
    status_mongo = {"status": "offline", "description": "error", 'lastUpdate': _tempo_atual()}
    post_api('mongodb', mongo_name, status_mongo)
    return True

def post_api(task, site, payload):
    url = 'http://10.7.118.220:8081/deadline/' + task + '/cadastrar/' + site
    headers = {'content-type': "application/json", 'cache-control': "no-cache"}
    try:
        requests.post(url, json=payload, headers=headers)
        return 201
    except Exception as e:
        gravar_log(e)
        return 404


def main(site):
    # Cria uma instancia da classe Influx e gera conexão
    cliente_influx = Influx()
    conexao_influx = cliente_influx.conectar_influxdb()
    # Dados da conexao com o MongoDB
    mongo_name = info_mongo[site]["name"]
    mongo_ip = info_mongo[site]["ip"]
    mongo_port = info_mongo[site]["port"]
    mongo_db = info_mongo[site]["db"]
    # Realiza um teste para verificar se a porta do mongoDB está aberta
    if checar_conexao_mongo(mongo_ip, mongo_port, mongo_name):
        gravar_log(f"MONGODB: Falha na conexao | Nome: {mongo_name} - IP: {mongo_ip} - DB: {mongo_db}")
        return False
    # Gerando instância do cliente Mongo, conectando e selecionando o banco
    cliente_mongo = Mongo(mongo_ip, mongo_port, mongo_db)
    conexao_mongo = cliente_mongo.conectar_mongo()
    banco_selecionado_mongo = cliente_mongo.usar_banco(conexao_mongo)

    # Exporta as colections do MongoDB
    dados_jobs = Collections(banco_selecionado_mongo).metricas_jobs()
    dados_slaves = Collections(banco_selecionado_mongo).metricas_slave_info()
    dados_pulse = Collections(banco_selecionado_mongo).metricas_pulse_info()
    dados_license = Collections(banco_selecionado_mongo).metricas_license_forwarder()

    # Faz os POST's para READ-API - Do outro lado, os posts são capturados e gravado no REDIS - Dados para o Zabbix
    try:
        post_api('jobs', mongo_name, dados_jobs)
        post_api('slaves', mongo_name, dados_slaves)
        if len(dados_pulse) == 1:
            post_api('pulses', mongo_name, dados_pulse[0])
        else:
            for pulse in dados_pulse: post_api('pulses_' + pulse["name"], mongo_name, pulse)
        post_api('licensesForwarders', mongo_name, dados_license)
    except Exception as e:
        gravar_log(f'[POST]:[ERRO][{e}]')

    # Importa os dados da collections para o InfluxDB - Os dados serão válidos para o Grafana
    try:
        Gravar(conexao_influx, mongo_name).importar('jobs', dados_jobs)
    except Exception as e:
        gravar_log(f'[INFLUXDB]:[JOBS][ERRO AO IMPORTAR][{e}]')
    try:
        Gravar(conexao_influx, mongo_name).importar('slaves', dados_slaves)
    except Exception as e:
        gravar_log(f'[INFLUXDB]:[SLAVES][ERRO AO IMPORTAR][{e}]')
    try:
        for pulse in dados_pulse: Gravar(conexao_influx, mongo_name).importar('pulses_' + pulse["name"], pulse)
    except Exception as e:
        gravar_log(f'[INFLUXDB]:[PULSES][ERRO AO IMPORTAR][{e}]')
    try:
        Gravar(conexao_influx, mongo_name).importar('license_forwarder', dados_license)
    except Exception as e:
        gravar_log(f'[INFLUXDB]:[LICENSE FORWARDER][ERRO AO IMPORTAR][{e}]')

    # Fecha conexão do site MongoDB
    cliente_mongo.fechar_mongo(conexao_mongo)

    # Encerrando conexao do InfluxDB
    cliente_influx.fechar_influxdb(conexao_influx)


if '__main__' == __name__:
    logging.basicConfig(filename="./logs/erros.log", filemode='a', datefmt='%d-%b-%y %H:%M:%S',
                        format='%(asctime)s - %(message)s', level=logging.INFO)
    DELAY = 60
    while True:
        for i in range(_total_sites()):
            worker = threading.Thread(target=main, args=(i,))
            worker.start()
        time.sleep(DELAY)