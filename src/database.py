from influxdb import InfluxDBClient
from pymongo import MongoClient
from src.variaveis import info_influxdb

class Mongo:
    def __init__(self, host, port, model):
        self.host = host
        self.port = port
        self.model = model

    def conectar_mongo(self):
        return MongoClient(self.host, self.port, serverSelectionTimeoutMS=5000)

    def fechar_mongo(self, client):
        client.close()

    def usar_banco(self, client):
        return client[self.model]


class Influx:
    def __init__(self, host=info_influxdb['host'], port=info_influxdb['port']):
        self.host = host
        self.port = port

    def conectar_influxdb(self):
        return InfluxDBClient(host=self.host, port=self.port)

    def fechar_influxdb(self, client):
        client.close()