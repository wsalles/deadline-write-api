
class Gravar:
    def __init__(self, client, database):
        self.banco = database
        self.conexao = client

    def check_banco(self):
        db_list = self.conexao.get_list_database()
        for db in db_list:
            if db['name'] == self.banco:
                return True
        return False

    def criar_banco(self):
        if self.check_banco():
            return self.conexao.switch_database(self.banco)
        try:
            self.conexao.create_database(self.banco)
            print('INFLUXDB: Banco criado!')
        except:
            pass
        finally:
            return self.conexao.switch_database(self.banco)

    def importar(self, measurement, lista):
        try:
            self.criar_banco()
        except Exception as e:
            print(e)
        json_body = [{"measurement": measurement, "fields": lista}]
        return self.conexao.write_points(json_body)