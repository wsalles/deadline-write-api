
info_mongo = [
    {"name": "deadline_emissora", "ip": '10.1.0.42', "port": 27100, 'db': 'deadline10db'},
    {"name": "deadline_estudios", "ip": '10.228.106.22', "port": 27100, 'db': 'deadline10db'},
    {"name": "deadline_jor_esp", "ip": '10.1.208.30', "port": 27100, 'db': 'deadline10db'},
    {"name": "deadline_com_pro", "ip": '10.161.0.40', "port": 27100, 'db': 'deadline10db'}
]

info_influxdb = {'host': '10.7.118.220', 'port': 8086}

db_influxdb = ['deadline_emissora', 'deadline_estudios', 'deadline_jor_esp', 'deadline_com_pro']