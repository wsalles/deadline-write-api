import datetime, time, logging

def _tempo_atual():
    t = time.time()
    return datetime.datetime.fromtimestamp(t).strftime('%d/%m/%Y %H:%M:%S')

def gravar_log(message):
    logging.info(message)

class Collections:
    def __init__(self, client):
        self.client = client
        logging.basicConfig(filename="./logs/erros.log", filemode='a', datefmt='%d-%b-%y %H:%M:%S',
                            format='%(asctime)s - %(message)s', level=logging.INFO)

    def jobs(self):
        return self.client['Jobs'].find()

    def license_forwarder(self):
        return self.client['LicenseForwarderInfoCollection'].find()

    def slave_info(self):
        return self.client['SlaveInfo'].find()

    def pulse_info(self):
        return self.client['PulseInfo'].find()


    def metricas_jobs(self):
        renderando, fila, erro = 0, 0, 0
        alerta = False
        try:
            dados = self.jobs()
            for job in range(dados.count()):
                if dados[job]["Stat"] == 1:
                    if dados[job]["RenderingChunks"] != 0:
                        renderando += 1
                    else:
                        fila += 1
                    # Verifica se um job possui erros superiores à 10
                    if erro < int(dados[job]["Errs"]):
                        erro = int(dados[job]["Errs"])
                        if erro >= 10:
                            alerta = True
            return {"alert": str(alerta), "error": erro, "rendering": renderando, "queued": fila,
                    'lastUpdate': _tempo_atual()}
        except Exception as e:
            gravar_log(f'[JOB]:[ERRO][{e}]')
        return {"alert": str(alerta), "error": erro, "rendering": renderando, "queued": fila,
                'lastUpdate': _tempo_atual()}

    def metricas_license_forwarder(self):
        lastTime, statTime, stat = 0, 0, 0
        status = 'unknown'
        try:
            dados = self.license_forwarder()
            for lic in range(dados.count()):
                lastTime = dados[lic]['LastWriteTime'].strftime("%Y/%m/%d %H:%M:%S")
                statTime = dados[lic]['StatTime'].strftime("%Y/%m/%d %H:%M:%S")
                stat = dados[lic]['Stat']
                if stat == 1:
                    status = 'running'
                elif stat == 2:
                    status = 'offline'
                elif stat == 3:
                    status = 'stalled'
                return {"lastWriteTime": lastTime, "statTime": statTime, "stat": stat, "status": status,
                        'lastUpdate': _tempo_atual()}
        except Exception as e:
            gravar_log(f'[LICENSE FORWARDER]:[ERRO][{e}]')
        return {"lastWriteTime": lastTime, "statTime": statTime, "stat": stat, "status": status,
                'lastUpdate': _tempo_atual()}

    def metricas_slave_info(self):
        datetimeFormat = '%Y/%m/%d %H:%M:%S'
        rendering, idle, offline, stalled = 0, 0, 0, 0
        alert = False
        lastRunningDate, minutesNotRunning = 0, 0
        try:
            dados = self.slave_info()
            for slave in range(dados.count()):
                if dados[slave]["Stat"] == 1:
                    rendering += 1
                elif dados[slave]["Stat"] == 2:
                    idle += 1
                elif dados[slave]["Stat"] == 3:
                    if "VFX" not in dados[slave]["Name"]:
                        offline += 1
                        alert = True
                elif dados[slave]["Stat"] == 4:
                    if "VFX" not in dados[slave]["Name"]:
                        stalled += 1
                        alert = True
            if rendering >= 1:
                lastRunningDate = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                minutesNotRunning = "0"
            else:
                lastRunningDate = "0"
                minutesNotRunning = "0"
            return {"alert": str(alert), "idle": idle, "offline": offline, "rendering": rendering, "stalled": stalled,
                    "lastRunningDate": lastRunningDate, "minutesNotRunning": minutesNotRunning,
                    'lastUpdate': _tempo_atual()}
        except Exception as e:
            gravar_log(f'[SLAVES]:[ERRO][{e}]')
        return {"alert": str(alert), "idle": idle, "offline": offline, "rendering": rendering, "stalled": stalled,
                "lastRunningDate": lastRunningDate, "minutesNotRunning": minutesNotRunning,
                'lastUpdate': _tempo_atual()}

    def metricas_pulse_info(self):
        alert = False
        status_pulse, name = '', ''
        stat = 0
        pulses = []
        try:
            dados = self.pulse_info()
            for pulse in range(dados.count()):
                stat = dados[pulse]["Stat"]
                name = dados[pulse]["_id"]
                if "stack" in name.lower():
                    name = "stack"
                else:
                    name = "manager"
                if stat == 0:
                    status_pulse = 'unknown'
                    alert = True
                elif stat == 1:
                    status_pulse = 'running'
                elif stat == 2:
                    status_pulse = 'offline'
                    alert = True
                elif stat == 4:
                    status_pulse = 'stalled'
                    alert = True
                pulses.append({"name": name, "alert": str(alert), "status": status_pulse, "stat": stat,
                               'lastUpdate': _tempo_atual()})
            return pulses
        except Exception as e:
            gravar_log(f'[PULSE]:[ERRO][{e}]')
        return {"name": name, "alert": str(alert), "status": status_pulse, "stat": stat, 'lastUpdate': _tempo_atual()}