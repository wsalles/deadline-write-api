FROM python:3-alpine

RUN apk add --virtual .build-dependencies \
            --no-cache \
            python3-dev \
            build-base \
            linux-headers \
            pcre-dev

RUN apk add --no-cache pcre

WORKDIR /app

COPY ./requirements.txt .
RUN pip install -r requirements.txt

ENV TZ="America/Sao_Paulo"
RUN apk add tzdata

RUN apk del .build-dependencies && rm -rf /var/cache/apk/*

RUN mkdir logs
COPY ./src ./src
COPY ./run.py .


CMD ["python3", "/app/run.py"]